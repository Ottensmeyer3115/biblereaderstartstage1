package bibleReader.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;

/*
 * Tests for the Reference class.
 * @author ?
 */

// Make sure you test at least the following:
// --All of the constructors and all of the methods.
// --Using a full title or an abbreviation for a book name.
// --The equals for success and for failure, especially when they match in 2 of the 3 places
//    (e.g. same book and chapter but different verse, same chapter and verse but different book, etc.)
// --That compareTo is ordered properly for similar cases as the equals tests.
//    (e.g. should Genesis 1:1 compareTo Revelation 22:21 return a positive or negative number?)
// --Any other potentially tricky things.

public class Stage01StudentReferenceTest {

	// A few sample references to get you started.
	private Reference	ruth1_1;
	private Reference	ruth1_2;
	private Reference	gen1_1;
	private Reference	rev1_1;
	
	/*
	 * Anything that should be done before each test.
	 * For instance, you might create some objects here that are used by several of the tests.
	 */
	@Before
	public void setUp() throws Exception {
		// A few sample references to get you started.
		ruth1_1 = new Reference(BookOfBible.Ruth, 1, 1);
		ruth1_2 = new Reference(BookOfBible.Ruth, 1, 2);
		gen1_1 = new Reference("Genesis", 1, 1);
		rev1_1 = new Reference(BookOfBible.Revelation, 1, 1);
		
		// TODO Create more objects that the tests will use here.
		// You need to make them fields so they can be seen in the test methods.
	}

	/*
	 * Anything that should be done at the end of each test.  
	 * Most likely you won't need to do anything here.
	 */
	@After
	public void tearDown() throws Exception {
		// You probably won't need anything here.
	}

	/*
	 * Add as many test methods as you think are necessary. Two suggestions (without implementation) are given below.	 */
	@Test
	public void testEquals() {
		assertTrue(ruth1_1.equals(new Reference("Ruth", 1, 1)));
		assertFalse(ruth1_1.equals(ruth1_2));
		assertFalse(ruth1_1.equals(gen1_1));
	}
	
	
	/*
	 * Method that will test the HashCode Method in the Reference Class.
	 */
	@Test
	public void testHashCode() {
		assertTrue(ruth1_1.compareTo(gen1_1) < 0);
		assertTrue(ruth1_1.compareTo(rev1_1) > 0);
		assertTrue(ruth1_1.compareTo(ruth1_1) == 0);
	}
	
	
	/*
	 * This method tests the first constructor in Reference Class.
	 */
	@Test
	public void testFirstConstructor(){
		 assertEquals("Genesis", gen1_1.getBook()); 
		 assertEquals(BookOfBible.Genesis, gen1_1.getBookOfBible());
		//tests constructors ability to implement getter methods.
		 assertEquals(1, gen1_1.getChapter());
		 assertEquals(1, gen1_1.getVerse());
		
	}
	
	/*
	 * This method tests the second constructor in Reference Class.
	 */
	@Test
	public void testSecondConstructor(){
		 assertEquals(ruth1_1, ruth1_1.getBook());
		 assertEquals(BookOfBible.Ruth, ruth1_1.getBookOfBible());
		 //tests constructors ability to implement getter methods.
		 assertEquals(1, ruth1_1.getChapter());
		 assertEquals(1, ruth1_1.getVerse());
	}

	@Test
	public void testCompareTo() {
		assertTrue(ruth1_1.compareTo(gen1_1) < 0);
		assertTrue(ruth1_1.compareTo(rev1_1) > 0);
		assertTrue(ruth1_1.compareTo(ruth1_1) == 0);
		
	}
	
	@Test
	public void testGetBook() {
		assertEquals("Revelation", rev1_1.getBook());
	}
	
	@Test
	public void testGetBookOfBible() {
		assertEquals(BookOfBible.Ruth, ruth1_1.getBookOfBible());
	}
	
	@Test
	public void testGetChapter() {
		assertEquals(1, ruth1_1.getChapter());
	
	}
	
	@Test
	public void testGetVerse() {
		assertEquals(1, ruth1_1.getVerse());
		
	}
	
	@Test
	public void testToString() {
		Reference john2_4 = new Reference(BookOfBible.John, 2, 4);
		assertEquals("John 2:4", john2_4.toString());
	}

}
